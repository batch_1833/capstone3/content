import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Employee Development Solutions",
		content: "Develop an in-depth knowledge about a series of implementation strategies and approaches.",
		destination: "/courses",
		// label: "Enroll"
	}

	return(
		<>
			<Banner data={data}/>
        	<Highlights />
		</>
	)
}