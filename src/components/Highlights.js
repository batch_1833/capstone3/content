//Applying bootstrap grid system
import {Row, Col, Card} from "react-bootstrap";

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight1 p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Professional Training Program</h2>
		                </Card.Title>
		                <Card.Text>
		                    Professional training programs provide opportunities for employees to refine skills, learn new capabilities, and perhaps most often, expand their networks of influential people. Whether you pursue internal or external programs, they provide a unique opportunity to apply newly-learned skills and processes immediately, appropriating them as needed for the demands of your role.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight2 p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Professional Diploma Programs</h2>
		                </Card.Title>
		                <Card.Text>
		                    As used in this Agreement, means the series of courses offered and taught by, relating to professional preparation, training and internships for your career.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight3 p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>In House Training</h2>
		                </Card.Title>
		                <Card.Text>
		                    Another benefit of in-house training is collaboration. Employees have the opportunity to collaborate with coworkers and develop stronger relationships with other team members. Consider how you can get managers involved, which experts say is one of the best ways to encourage employee retention.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    
		</Row>
	)
}